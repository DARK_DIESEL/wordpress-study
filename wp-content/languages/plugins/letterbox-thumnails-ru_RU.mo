��          �   %   �      0  Z   1     �  ,   �     �     �     �     �     �     	          4     I     g     l     �  -   �  2   �     �     �       �   /  2   �  0   �  w    �   �     K  U   ^     �     �     �  I   �     >      ^  8        �  8   �     	  1   	  %   I	  ^   o	  �   �	     Y
  5   l
  +   �
  �   �
  5   �  0   �                                                                         
                                    	                  <b class="color-note">NOTE!</b> For selected image sizes cropping option will be ignoring! Background color: Could not calculate resized image dimensions Cropping Donate Igor Peshkov Image resize failed. Image sizes Image sizes: LetterBox Thumbnails Settings Letterbox Thumbnails Letterbox Thumbnails Settings Name Permission check failed Save settings Select color that will be used as background. Select image sizes for that apply letterbox style. Settings Settings successfully saved! Size (width x height) This plugin add new editor for generating thumbnails with letterbox style. Background color for letterbox style sets in settings. https://wordpress.org/plugins/letterbox-thumnails/ https://www.facebook.com/igor.peshkov.27.07.1988 PO-Revision-Date: 2019-05-22 15:34:38+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Generator: GlotPress/2.4.0-alpha
Language: ru
Project-Id-Version: Plugins - Letterbox Thumbnails - Stable (latest release)
 <b class="color-note">Обратите внимание!</b> Для выбранных размеров картинки опция обрезки будет проигнорирована! Цвет фона: Не удалось вычислить новый размер изображения Обрезка Пожертвовать Игорь Пешков Не удалось изменить размер изображения. Размеры картинок Размеры картинок: Настройки Леттербокс Миниатюр Letterbox Thumbnails Настройки Леттербокс Миниатюр Название Не удалось проверить права Сохранить настройки Выберете цвет который будет использоваться как фон Выберите размеры картинок для которых будет применяться  леттербокс стиль. Настройки Настройки успешно сохранены! Размер (ширина х высота) Плагин добавляет новый редактор для генерации миниатюр с леттербокс стилем. Фон для леттербокс стиля задается в настройках. https://ru.wordpress.org/plugins/letterbox-thumnails/ https://www.facebook.com/igor.peshkov.27.07.1988 